package jogodacobrinha;

import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener; // todos os imports necessários
import java.awt.event.KeyListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ModoComum extends JPanel implements KeyListener, ActionListener{
	
	private int[] tremComprimentox = new int[750]; // declara os vetores de localização do trem
	private int[] tremComprimentoy = new int[750];
	
	private boolean esquerda = false; // declara os booleanos que serão escritos pelo teclado.
	private boolean direita = false;
	private boolean cima = false;
	private boolean baixo = false;
	private static boolean pegouPare = false;
	
	private ImageIcon indoDireita; // declara os ícones usados para representar o trem.
	private ImageIcon indoEsquerda; // para cada direção, o ícone usado é diferente.
	private ImageIcon indoCima;
	private ImageIcon indoBaixo;
	
	private int tremComprimento = 2; // tamanho inicial do trem (locomotiva + 1 vagão)
	
	private int mexeu = 0; // variável que armazena a movimentação do trem.
	
	private int tempoItem; // variável que armazena o tempo que os itens ("frutas") estarão na tela.
	
	// declara os vetores para o sorteio da posição que cada item (fruta) ficará.
	private int[] ticketPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] ticketDuploPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] estacaoPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] parePosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	
	private int[] itemPosicaoy = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};

	private ImageIcon ticketIcone; // declaração das variáveis que armazenam os ícones dos itens (frutas) 
	private ImageIcon ticketDuploIcone;
	private ImageIcon estacaoIcone;
	private ImageIcon pareIcone;
	
	private ImageIcon barreiraIcone; // variáveis relacionadas à barreira
	private int[] barreiraPosicaox = {375, 400, 425, 450, 475};
	private int[] barreiraPosicaoy = {275, 300, 325, 350, 375};
		
	private Random random = new Random(); // função random, que escolhe o local de spawn das frutas
	private int posicaox = random.nextInt(34);
	private int posicaoy = random.nextInt(23);
	
	private Random itemRandom = new Random();
	private int item = itemRandom.nextInt(15);
	
	private int pontuacao = 0;
		
	private Timer timer; // variáveis para movimentação do trem
	private int delay = 100; 
	private ImageIcon vagao;
	
	private ImageIcon imagemTitulo;
	
	public ModoComum() // inicializa as particularidades do modo comum
	{	
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay,this);
		timer.start();
		tempoItem = 0;	
	}
	public void paint(Graphics g) // ilustra a janela do modo comum na tela
	{
		if(mexeu == 0) {
			tremComprimentox[2] = 50; // posição inicial do trem
			tremComprimentox[1] = 75;
			tremComprimentox[0] = 100;
			
			tremComprimentoy[2] = 100;
			tremComprimentoy[1] = 100;
			tremComprimentoy[0] = 100;
		}
		
		// ilusta a parte externa da janela (bordas e topo)

		g.setColor(Color.gray);
		g.drawRect(24, 10, 851, 55);
		
		imagemTitulo = new ImageIcon("icones/titulo.png"); // badge que ilustra o título do game
		imagemTitulo.paintIcon(this, g, 25, 11);
		
		g.setColor(Color.LIGHT_GRAY);
		g.drawRect(24, 74, 851, 577);
		
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(25, 75, 850, 575);
		
		// ilustra o placar

		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14)); 
		g.drawString("Pontuação: "+pontuacao, 750, 30);
		
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Vagões: "+tremComprimento, 750, 50);
		
		indoDireita = new ImageIcon("icones/locomotiva_direita.png");
		indoDireita.paintIcon(this, g, tremComprimentox[0], tremComprimentoy[0]);

		// laço responsável pela mudança de ícone conforme a direção
		
		for (int a = 0; a < tremComprimento; a++) {
			if(a==0 && direita) {
				indoDireita = new ImageIcon("icones/locomotiva_direita.png");
				indoDireita.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && esquerda) {
				indoEsquerda = new ImageIcon("icones/locomotiva_esquerda.png");
				indoEsquerda.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && cima) {
				indoCima = new ImageIcon("icones/locomotiva_cima.png");
				indoCima.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && baixo) {
				indoBaixo = new ImageIcon("icones/locomotiva_baixo.png");
				indoBaixo.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a!=0) {
				vagao = new ImageIcon("icones/vagao.png");
				vagao.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
		}

		// algoritmo relacionado ao ticket simples (SimpleFruit)
        if (item <= 6) {
        	
            ticketIcone = new ImageIcon("icones/ticket.png");
            
            if ((ticketPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                pontuacao++; // cresce um ponto
                tremComprimento++; // cresce um vagão
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
            }
            else if (tempoItem >= 60) 
            {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            ticketIcone.paintIcon(this, g, ticketPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }

		// algoritmo relacionado ao ticket duplo (BigFruit)
        else if (item > 6 && item <= 9 ) {
            ticketDuploIcone = new ImageIcon("icones/ticket_duplo.png");
            if ((ticketDuploPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                pontuacao+=2; // ganha dois pontos
                tremComprimento++; // cresce um vagão
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
            }
            else if (tempoItem >= 60) {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            ticketDuploIcone.paintIcon(this, g, ticketDuploPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }

		// algoritmo relacionado ao sinal de pare (BombFruit)
        else if (item > 9 && item <= 13 ) {
        	
            pareIcone = new ImageIcon("icones/pare.png");
            
            if ((parePosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                direita = false;
                esquerda = false;
                cima = false;
                baixo = false;
                pegouPare = true; // finaliza o jogo, e mostra o placar.

                g.setColor(Color.RED);
				g.setFont(new Font("arial", Font.ITALIC, 50));
				g.drawString("Você perdeu!", 300, 150);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Sua pontuação: "+pontuacao, 300, 180);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Pressione 'R' para reiniciar.", 300, 200);

            }
            else if (tempoItem >= 60) {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            pareIcone.paintIcon(this, g, parePosicaox[posicaox], itemPosicaoy[posicaoy]);
        }

		// algoritmo relacionado à estação (DecreaseFruit)
        else {
        	
            estacaoIcone = new ImageIcon("icones/estacao.png");
            
            if ((estacaoPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                tremComprimento = 2; // reinicia o tamanho do trem, e não mexe nas variáveis de pontuação
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
                
            }
            else if (tempoItem >= 60)
            {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            estacaoIcone.paintIcon(this, g, estacaoPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }
        
        barreiraIcone = new ImageIcon("icones/animais.png"); // ilustra a barreira de animais na via férrea.
        barreiraIcone.paintIcon(this, g, 375, 275);

		for(int b = 1; b < tremComprimento; b++) {
			if(((tremComprimentox[b] == tremComprimentox[0]) && (tremComprimentoy[b] == tremComprimentoy[0])) || ((tremComprimentox[b] >= barreiraPosicaox[0] && tremComprimentox[b] <= barreiraPosicaox[4] ) &&
                    (tremComprimentoy[b] >= barreiraPosicaoy[0] && tremComprimentoy[b] <= barreiraPosicaoy[4]))) {
				direita = false; 
				esquerda = false;  // condicional para fim de jogo:
				cima = false;	   // 1. O jogador colidiu com o seu próprio corpo.
				baixo = false;     // 2. O jogador colidiu com a barreira de animais.
				pegouPare = false;
				
				g.setColor(Color.RED);
				g.setFont(new Font("arial", Font.ITALIC, 50)); // mostra a pontuação final
				g.drawString("Você perdeu!", 300, 150);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Sua pontuação: "+pontuacao, 300, 180);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Pressione 'R' para reiniciar.", 300, 200);
				
				
			}
		}
		
		g.dispose();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) { // função que altera as variáveis de localização do trem, após receber o comando pelas teclas.
		timer.start();
		tempoItem++;
		
		if(direita) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentoy[r+1] = tremComprimentoy[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentox[r] = tremComprimentox[r] + 25;
				}
				else {
					tremComprimentox[r] = tremComprimentox[r-1];
				}
				if(tremComprimentox[r]>850) {
					tremComprimentox[r] = 850;
				}
			}
			repaint();			
		}
		if(esquerda) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentoy[r+1] = tremComprimentoy[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentox[r] = tremComprimentox[r] - 25;
				}
				else {
					tremComprimentox[r] = tremComprimentox[r-1];
				}
				if(tremComprimentox[r] < 25) {
					tremComprimentox[r] = 25;
				}
			}
			repaint();
		}
		if(cima) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentox[r+1] = tremComprimentox[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentoy[r] = tremComprimentoy[r] - 25;
				}
				else {
					tremComprimentoy[r] = tremComprimentoy[r-1];
				}
				if(tremComprimentoy[r] < 75) {
					tremComprimentoy[r] = 75;
				}
			}
			repaint();			
		}
		if(baixo) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentox[r+1] = tremComprimentox[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentoy[r] = tremComprimentoy[r] + 25;
				}
				else {
					tremComprimentoy[r] = tremComprimentoy[r-1];
				}
				if(tremComprimentoy[r] > 625) {
					tremComprimentoy[r] = 625;
				}
			}
			repaint();		
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	
	}
	
	@Override
	public void keyPressed(KeyEvent e) {  // função que recebe a movimentação do trem pelas teclas direcionais.
		if(e.getKeyCode() == KeyEvent.VK_R) {
			mexeu = 0;
			pontuacao = 0;
			tremComprimento = 2;
			pegouPare = false;
			tempoItem = 0;
			repaint();
		}
		
		if(!pegouPare) {
			
			if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
				mexeu++;
				direita = true;
				if(!esquerda) {
					direita = true;
				}
				else {
					direita = false;
					esquerda = true;
				}
				cima = false;
				baixo = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_LEFT) {
				mexeu++;
				esquerda = true;
				if(!direita) {
					esquerda = true;
				}
				else {
					esquerda = false;
					direita = true;
				}
				cima = false;
				baixo = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_UP) {
				mexeu++;
				cima = true;
				if(!baixo) {
					cima = true;
				}
				else {
					cima = false;
					baixo = true;
				}
				esquerda = false;
				direita = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_DOWN) {
				mexeu++;
				baixo = true;
				if(!cima) {
					baixo = true;
				}
				else {
					cima = true;
					baixo = false;
				}
				esquerda = false;
				direita = false;
			}
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		
	}
	
}
