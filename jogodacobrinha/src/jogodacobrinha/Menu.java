package jogodacobrinha;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Menu { // cria o menu inicial, declarando as bibliotecas a serem usadas e inicializa os métodos

   private JFrame mainFrame;
   private JLabel headerLabel;
   private JLabel statusLabel;
   private JPanel controlPanel;

   public Menu(){
      preparaGUI();
   }

   public static void main(String[] args){
	   Menu menu = new Menu();  
	   menu.mostrarJanela();       
   }
      
   private void preparaGUI(){  // método que cria a inteface gráfica do menu inicial
      mainFrame = new JFrame("take the wagons!");
      mainFrame.setSize(400,400);
      mainFrame.setLayout(new GridLayout(3, 1));

      headerLabel = new JLabel("",JLabel.CENTER );
      statusLabel = new JLabel("",JLabel.CENTER);        

      statusLabel.setSize(350,100);
      mainFrame.addWindowListener(new WindowAdapter() {
         public void windowClosing(WindowEvent windowEvent){
	        System.exit(0);
         }        
      });    
      controlPanel = new JPanel();
      controlPanel.setLayout(new FlowLayout());

      mainFrame.add(headerLabel);
      mainFrame.add(controlPanel);
      mainFrame.add(statusLabel);
      mainFrame.setVisible(true);  
   }

   private void mostrarJanela(){ 
      headerLabel.setText("Selecione uma opção abaixo:"); 

      JButton comumButton = new JButton("Modo comum"); // botões que o usuário poderá interagir
      JButton kittyButton = new JButton("Modo kitty");
      JButton starButton = new JButton("Modo star");

      comumButton.setActionCommand("Modo comum"); 
      kittyButton.setActionCommand("Modo kitty");
      starButton.setActionCommand("Modo star");

      comumButton.addActionListener(new ButtonClickListener());  // recebe a ação pelo click do mouse.
      kittyButton.addActionListener(new ButtonClickListener()); 
      starButton.addActionListener(new ButtonClickListener()); 

      controlPanel.add(comumButton);
      controlPanel.add(kittyButton);
      controlPanel.add(starButton);       

      mainFrame.setVisible(true); 
   }

   private class ButtonClickListener implements ActionListener{
      public void actionPerformed(ActionEvent e) {
         String command = e.getActionCommand();  
         if( command.equals( "Modo comum" ))  { // de acordo com o botão pressionado, incializa o modo de jogo solicitado
        	JFrame janela = new JFrame();
     		ModoComum modocomum = new ModoComum();
     		
     		janela.setBounds(10, 10, 905, 700);
     		janela.setBackground(Color.gray);
     		janela.setResizable(false);
     		janela.setVisible(true);
     		janela.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     		janela.add(modocomum);
         }
         else if( command.equals( "Modo kitty" ) )  {
        	JFrame janela1 = new JFrame();
     		ModoKitty modokitty = new ModoKitty();
     		
     		janela1.setBounds(10, 10, 905, 700);
     		janela1.setBackground(Color.cyan);
     		janela1.setResizable(false);
     		janela1.setVisible(true);
     		janela1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     		janela1.add(modokitty);
         }
         else  {
        	JFrame janela2 = new JFrame();
     		ModoStar modostar = new ModoStar();
     		
     		janela2.setBounds(10, 10, 905, 700);
     		janela2.setBackground(Color.yellow);
     		janela2.setResizable(false);
     		janela2.setVisible(true);
     		janela2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
     		janela2.add(modostar);
         }  	
      }		
   }
}

