package jogodacobrinha;

import java.awt.Font;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.Random;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.Timer;

public class ModoKitty extends JPanel implements KeyListener, ActionListener{
	
	private int[] tremComprimentox = new int[750];
	private int[] tremComprimentoy = new int[750];
	
	private boolean esquerda = false;
	private boolean direita = false;
	private boolean cima = false;
	private boolean baixo = false;
	private static boolean pegouPare = false;
	
	private ImageIcon indoDireita;
	private ImageIcon indoEsquerda;
	private ImageIcon indoCima;
	private ImageIcon indoBaixo;
	
	private int tremComprimento = 2;
	
	private int mexeu = 0;
	
	private int tempoItem;
	
	private int[] ticketPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] ticketDuploPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] estacaoPosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	private int[] parePosicaox = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750, 775, 800, 825, 850};
	
	private int[] itemPosicaoy = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325, 350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};

	private ImageIcon ticketIcone;
	private ImageIcon ticketDuploIcone;
	private ImageIcon estacaoIcone;
	private ImageIcon pareIcone;
	
	private ImageIcon barreiraIcone; // neste modo, apenas a imagem da barreira é declarada, pois o trem sempre irá atravessá-la.
		
	private Random random = new Random();
	private int posicaox = random.nextInt(34);
	private int posicaoy = random.nextInt(23);
	
	private Random itemRandom = new Random();
	private int item = itemRandom.nextInt(15);
	
	private int pontuacao = 0;
		
	private Timer timer;
	private int delay = 100;
	private ImageIcon vagao;
	
	private ImageIcon imagemTitulo;
	
	public ModoKitty() 
	{	
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		timer = new Timer(delay,this);
		timer.start();
		tempoItem = 0;	
	}
	public void paint(Graphics g) 
	{
		if(mexeu == 0) {
			tremComprimentox[2] = 50;
			tremComprimentox[1] = 75;
			tremComprimentox[0] = 100;
			
			tremComprimentoy[2] = 100;
			tremComprimentoy[1] = 100;
			tremComprimentoy[0] = 100;
		}
		
		g.setColor(Color.gray);
		g.drawRect(24, 10, 851, 55);
		
		imagemTitulo = new ImageIcon("icones/titulo_fantasma.png"); // ícones próprios para cada modo.
		imagemTitulo.paintIcon(this, g, 25, 11);
		
		g.setColor(Color.LIGHT_GRAY);
		g.drawRect(24, 74, 851, 577);
		
		g.setColor(Color.LIGHT_GRAY);
		g.fillRect(25, 75, 850, 575);
		
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Pontuação: "+pontuacao, 750, 30);
		
		g.setColor(Color.white);
		g.setFont(new Font("arial", Font.PLAIN, 14));
		g.drawString("Vagões: "+tremComprimento, 750, 50);
		
		indoDireita = new ImageIcon("icones/locomotiva_direita_fantasma.png");
		indoDireita.paintIcon(this, g, tremComprimentox[0], tremComprimentoy[0]);
		
		for (int a = 0; a < tremComprimento; a++) {
			if(a==0 && direita) {
				indoDireita = new ImageIcon("icones/locomotiva_direita_fantasma.png");
				indoDireita.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && esquerda) {
				indoEsquerda = new ImageIcon("icones/locomotiva_esquerda_fantasma.png");
				indoEsquerda.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && cima) {
				indoCima = new ImageIcon("icones/locomotiva_cima_fantasma.png");
				indoCima.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a==0 && baixo) {
				indoBaixo = new ImageIcon("icones/locomotiva_baixo_fantasma.png");
				indoBaixo.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
			if(a!=0) {
				vagao = new ImageIcon("icones/vagao_fantasma.png");
				vagao.paintIcon(this, g, tremComprimentox[a], tremComprimentoy[a]);
			}
		}

		// ---------------------------------
		
        if (item <= 6) {
        	
            ticketIcone = new ImageIcon("icones/ticket.png");
            
            if ((ticketPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                pontuacao++;
                tremComprimento++;
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
            }
            else if (tempoItem >= 60) 
            {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            ticketIcone.paintIcon(this, g, ticketPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }
        else if (item > 6 && item <= 9 ) {
            ticketDuploIcone = new ImageIcon("icones/ticket_duplo.png");
            if ((ticketDuploPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                pontuacao+=2;
                tremComprimento++;
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
            }
            else if (tempoItem >= 60) {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            ticketDuploIcone.paintIcon(this, g, ticketDuploPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }
        else if (item > 9 && item <= 13 ) {
        	
            pareIcone = new ImageIcon("icones/pare.png");
            
            if ((parePosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                direita = false;
                esquerda = false;
                cima = false;
                baixo = false;
                pegouPare = true;

                g.setColor(Color.RED);
				g.setFont(new Font("arial", Font.ITALIC, 50));
				g.drawString("Você perdeu!", 300, 150);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Sua pontuação: "+pontuacao, 300, 180);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Pressione 'R' para reiniciar.", 300, 200);

            }
            else if (tempoItem >= 60) {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            pareIcone.paintIcon(this, g, parePosicaox[posicaox], itemPosicaoy[posicaoy]);
        }
        else {
        	
            estacaoIcone = new ImageIcon("icones/estacao.png");
            
            if ((estacaoPosicaox[posicaox] == tremComprimentox[0] && itemPosicaoy[posicaoy] == tremComprimentoy[0])) {
                tremComprimento = 2;
                tempoItem = 0;
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);

                item = itemRandom.nextInt(15);
                
            }
            else if (tempoItem >= 60)
            {
                posicaox = random.nextInt(29);
                posicaoy = random.nextInt(18);
                item = itemRandom.nextInt(15);
                tempoItem = 0;
            }
            estacaoIcone.paintIcon(this, g, estacaoPosicaox[posicaox], itemPosicaoy[posicaoy]);
        }
        
        barreiraIcone = new ImageIcon("icones/animais.png");
        barreiraIcone.paintIcon(this, g, 375, 275);

		for(int b = 1; b < tremComprimento; b++) {
			if((tremComprimentox[b] == tremComprimentox[0]) && (tremComprimentoy[b] == tremComprimentoy[0])) {
				direita = false;
				esquerda = false;
				cima = false;
				baixo = false;
				pegouPare = false;
				
				g.setColor(Color.RED);
				g.setFont(new Font("arial", Font.ITALIC, 50));
				g.drawString("Você perdeu!", 300, 150);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Sua pontuação: "+pontuacao, 300, 180);
				
				g.setColor(Color.DARK_GRAY);
				g.setFont(new Font("arial", Font.PLAIN, 20));
				g.drawString("Pressione 'R' para reiniciar.", 300, 200);
				
				
			}
		}
		
		g.dispose();
		
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		timer.start();
		tempoItem++;
		
		if(direita) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentoy[r+1] = tremComprimentoy[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentox[r] = tremComprimentox[r] + 25;
				}
				else {
					tremComprimentox[r] = tremComprimentox[r-1];
				}
				if(tremComprimentox[r]>850) {
					tremComprimentox[r] = 850;
				}
			}
			repaint();			
		}
		if(esquerda) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentoy[r+1] = tremComprimentoy[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentox[r] = tremComprimentox[r] - 25;
				}
				else {
					tremComprimentox[r] = tremComprimentox[r-1];
				}
				if(tremComprimentox[r] < 25) {
					tremComprimentox[r] = 25;
				}
			}
			repaint();
		}
		if(cima) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentox[r+1] = tremComprimentox[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentoy[r] = tremComprimentoy[r] - 25;
				}
				else {
					tremComprimentoy[r] = tremComprimentoy[r-1];
				}
				if(tremComprimentoy[r] < 75) {
					tremComprimentoy[r] = 75;
				}
			}
			repaint();			
		}
		if(baixo) {
			for(int r = tremComprimento-1; r>=0; r--) {
				tremComprimentox[r+1] = tremComprimentox[r];
			}
			for(int r = tremComprimento; r>=0; r--) {
				if(r==0) {
					tremComprimentoy[r] = tremComprimentoy[r] + 25;
				}
				else {
					tremComprimentoy[r] = tremComprimentoy[r-1];
				}
				if(tremComprimentoy[r] > 625) {
					tremComprimentoy[r] = 625;
				}
			}
			repaint();		
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		if(e.getKeyCode() == KeyEvent.VK_R) {
			mexeu = 0;
			pontuacao = 0;
			tremComprimento = 2;
			pegouPare = false;
			tempoItem = 0;
			repaint();
		}
		
		if(!pegouPare) {
			
			if(e.getKeyCode() == KeyEvent.VK_RIGHT) {
				mexeu++;
				direita = true;
				if(!esquerda) {
					direita = true;
				}
				else {
					direita = false;
					esquerda = true;
				}
				cima = false;
				baixo = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_LEFT) {
				mexeu++;
				esquerda = true;
				if(!direita) {
					esquerda = true;
				}
				else {
					esquerda = false;
					direita = true;
				}
				cima = false;
				baixo = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_UP) {
				mexeu++;
				cima = true;
				if(!baixo) {
					cima = true;
				}
				else {
					cima = false;
					baixo = true;
				}
				esquerda = false;
				direita = false;
			}
			if(e.getKeyCode() == KeyEvent.VK_DOWN) {
				mexeu++;
				baixo = true;
				if(!cima) {
					baixo = true;
				}
				else {
					cima = true;
					baixo = false;
				}
				esquerda = false;
				direita = false;
			}
		}
	}
	
	@Override
	public void keyReleased(KeyEvent e) {
		
	}
	
}
